@extends('layout.layout')
@php
$item  = $data["item"];



@endphp
@section('title', $item->title)

@section('content')
<div class="content service-detail">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/">Trang chủ</a></li>
            <li><a href="/instruct">Đào tạo</a></li>
            <li>{{ $item->title }}</li>
        </ul>
        <div class="box main-product border-line">
            <div class="overlay"></div>
            <span class="icon-ham" @click="showLeftMenu"><img src="./assets/images/icon_hamburger.png"></span>
            <div class="left-menu" style="display:none">
                <div class="menu-drop">
                    <h4 class="title">Danh mục</h4>
                    <ul class="menu">
                        <li v-for="item in listMenu"><a :href="'/service-detail?service_code='+item.code">@{{ item.title }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="right-product" style="width: 100%">
                <h4 class="title">{{ $item->title }}</h4>
                <div class="box-product">
                    <div class="box-info">
                        <p class="content-info">
                        {!!$item->content!!}

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
           
        },
        created() {
           
        },
        methods: {
            showLeftMenu() {
                $(".left-menu").slideToggle(400);
                $(".overlay").toggle(400);
            }
        }
    });
  </script>  
@endsection