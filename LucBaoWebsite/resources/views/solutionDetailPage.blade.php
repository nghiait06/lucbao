@extends('layout.layout')
@php
$item  = $data["item"];
$allSolution = $data["allSolution"];


@endphp
@section('title', $item->title)
@section('content')
<div class="content service-detail">
    <div class="container">
        <div class="banner">
            <span class="txt">Giải pháp</span>
            <img src="{{URL::asset('assets/images/img_service.jpg')}}">
        </div>
        <ul class="breadcrumb">
            <li><a href="/">Trang chủ</a></li>
            <li><a href="/giai-phap">Giải pháp</a></li>
            <li>{{ $item->title }}</li>
        </ul>
        <div class="box main-product border-line">
            <div class="overlay"></div>
            <span class="icon-ham" @click="showLeftMenu"><img src= "{{URL::asset('assets/images/icon_hamburger.png')}}"></span>
            <div class="left-menu">
                <div class="menu-drop">
                    <h4 class="title">Danh mục</h4>
                    <ul class="menu">
                    @foreach($allSolution as $item1)

                        <li><a href="/giai-phap/{{$item1->slug}}">{{ $item1->title }}</a></li>

                    @endforeach
                    </ul>
                </div>
            </div>
            <div class="right-product">
            <h4 class="title">{{ $item->title }}</h4>
                <div class="box-product">
                    <div class="box-info">
                        <p class="content-info" >
                           {!!$item->content!!}

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
         
        },
        created() {
            
        },
        methods: {
            showLeftMenu() {
                $(".left-menu").slideToggle(400);
                $(".overlay").toggle(400);
            }
        }
    });
  </script>  
@endsection