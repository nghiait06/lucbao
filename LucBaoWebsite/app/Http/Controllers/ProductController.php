<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\Brand;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all danh muc san pham

        $allProductCategory = ProductCategory::all();
        $allBrand = Brand::all();

        $data = array(
            'allProductCategory'=>$allProductCategory,
			'productCategorySelect' => null,
            'allBrand'=>$allBrand
            );
        // get all thuong hieu

        return view("ProductPage")->with('data',$data);
		
		
		
    }
	
	 public function GetDetailProduct(Request $request)
    {
        // get all news products

        // get product

    

        $code =$request->input('code');
        $data = Product::leftJoin('product_categories', 'products.productCategoryCode', '=', 'product_categories.id')
        ->where('products.slug', $code)
        ->select("products.*", "product_categories.name as product_categoriesName")
        ->first();
        return $data;
    }
	    public function GetDetail($urlName)
    {
		  $allProductCategory = ProductCategory::all();
        $allBrand = Brand::all();

		$item = ProductCategory::where('slug', $urlName)->get();
	
	    
		$data = array(
            'allProductCategory'=>$allProductCategory,
			'productCategorySelect' => $item,
            'allBrand'=>$allBrand
            );
        // get all thuong hieu
	  
	  
      return view("ProductPage")->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }


    public function GetAll(Request $request)
    {
        $productCategoryCode =$request->input('productCategoryCode');
        $brandCode =$request->input('brandCode');
        $orderByColum = $request->input ('orderByColum');
        $orderByvalue = $request->input ('orderByvalue');
        $data = Product::leftJoin('brands', 'products.brandCode', '=', 'brands.code')
        ->where('products.isActive', 1);
        if($productCategoryCode)
        {
            $data =$data->where('products.productCategoryCode',$productCategoryCode);
        }
        if($brandCode)
        {
            $data =$data->where('products.brandCode',$brandCode);
        }
        
        $data = $data->select('products.*','brands.name as brandName');
        if($orderByColum)
        {
            $data = $data->orderBy($orderByColum,$orderByvalue);
        }
        else 
        {
            $data = $data->orderBy('products.isSalest', 'desc')
            ->orderBy('products.updated_at', 'desc');
    
        }

        $data= $data->take(20);
        
        return $data->get();
    }



    public function getAllSpecial()
    {   
        
        return  Product::where('isActive',1)
           
             ->orderby('isSpecial','desc')
             ->orderby('created_at','desc')
            ->take(5)
            ->get();
            

    }



    public function GetDetail1($urlName)
    {

        $codeInput =$urlName;
       
        $item = null;
        if($codeInput)
        {
           
            $item = Product::leftJoin('product_categories', 'products.productCategoryCode', '=', 'product_categories.id')
            ->where('products.slug', $codeInput)
            ->select("products.*", "product_categories.name as product_categoriesName")
            ->first();

           
              if($item)
            {
                // get all product relation
                $item->imageLinks = json_decode($item->imageDetail);
  
                $allProductRelationship =  Product::join('product_categories', 'products.productCategoryCode', '=', 'product_categories.id')
                ->where('products.productCategoryCode','=',$item->productCategoryCode)
                ->where('products.code', '<>',$codeInput)
                ->select("products.*", "product_categories.name as product_categoriesName")
                ->orderBy('products.isSalest', 'desc')
                ->orderBy('products.created_at', 'desc')
                ->take(10)
                ->get();


                $allProductNews =  Product::leftJoin('product_categories', 'products.productCategoryCode', '=', 'product_categories.id')
                ->select("products.*", "product_categories.name as product_categoriesName")
                ->orderBy('products.created_at', 'desc')
                ->take(10)
                ->get();
                


          
                $data = [
                    'item'  => $item,
                    'allProductRelationship'=>$allProductRelationship,
                    'allProductNews'=>$allProductNews
                
                ];  
           
                return view('productDetailPage', ['data' => $data]);
            }
            else 

            {
                return  "not found";
            }
        }
        else 
        {
            return  "not found";
        }

    }


}
