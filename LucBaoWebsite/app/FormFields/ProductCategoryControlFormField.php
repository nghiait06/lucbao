<?php

namespace App\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class ProductCategoryControlFormField extends AbstractHandler
{
    protected $codename = 'ProductCategoryControl';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('ProductCategoryControl', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}