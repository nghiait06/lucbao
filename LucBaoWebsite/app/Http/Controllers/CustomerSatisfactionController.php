<?php

namespace App\Http\Controllers;

use App\CustomerSatisfaction;
use Illuminate\Http\Request;

class CustomerSatisfactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerSatisfaction  $customerSatisfaction
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerSatisfaction $customerSatisfaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerSatisfaction  $customerSatisfaction
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerSatisfaction $customerSatisfaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerSatisfaction  $customerSatisfaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerSatisfaction $customerSatisfaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerSatisfaction  $customerSatisfaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerSatisfaction $customerSatisfaction)
    {
        //
    }
}
