<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('services', function (Blueprint $table) {
			$table->string('urlShortName')->nullable();
		});
		
		Schema::table('instructs', function (Blueprint $table) {
			$table->string('urlShortName', 250)->nullable();
		;
		});
		Schema::table('solutions', function (Blueprint $table) {
			
				$table->string('urlShortName', 250)->nullable();
		
		});
		
		Schema::table('news', function (Blueprint $table) {
			
				$table->string('urlShortName', 250)->nullable();
		
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
