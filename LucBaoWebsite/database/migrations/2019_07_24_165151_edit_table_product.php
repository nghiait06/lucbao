<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('products', function (Blueprint $table) {
			$table->dropColumn('productCategoryCode');
		
			
		});
		
		Schema::table('products', function (Blueprint $table) {
			$table->boolean('isNew')->default(0);
			$table->boolean('isSpecial')->default(0);
			$table->integer('productCategoryCode')->nullable();
			$table->string('slug')->nullable()->unique();
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
