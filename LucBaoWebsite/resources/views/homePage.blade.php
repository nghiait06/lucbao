@extends('layout.layout')

@section('title', __('hompage.CompanyFullName') )
@php
$allService  = $data["allService"];
$allNews = $data["allNews"];
$allInstruct = $data["allInstruct"];
$allSolutiion = $data["allSolutiion"];

@endphp

@section('content')
<div class="content homePage">
    <div class="container">
        <div class="slider-header slider-top">
            <div class="slide" v-for="(link,index) in 4"><img :src="'./assets/images/img_slider_'+(index+1)+'.jpg'"></div>
        </div>
        <div class="box box-content border-line">
            <h2 class="title-box"><a href="/dich-vu">{{ __('hompage.serviceTitle') }}</a></h2>
       
            <div class="box-detail">
                <div class="col-detail" v-for="item in listAllServices" >
                    <div class="img"><img :src= "'/storage/'+item.iconLink"></div>
                    <div class="txt">
                        <h4 class="title-detail"><a :href="'/dich-vu/'+item.slug">@{{item.title}}</a></h4>
                        <p class="txt-detail">@{{item.description}}</p>
                    </div>
                </div>
            </div>
           
         
        </div>
        <div class="box box-content border-line">
            <h2 class="title-box"><a href="/dao-tao">{{ __('hompage.educationTitle') }}</a></h2>
            <div class="box-detail">
                <div class="col-detail" v-for="item in listAllInstruct" >
                  <div class="img"><img :src= "'/storage/'+item.iconLink"></div>
                    <div class="txt">
                        <h4 class="title-detail"><a :href="'/dao-tao/'+item.slug">@{{item.title}}</a></h4>
                        <p class="txt-detail">@{{item.description}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-content border-line">
            <h2 class="title-box"><a href="/giai-phap">{{ __('hompage.solutionService') }}</a></h2>
            <div class="box-detail">
                <div class="col-detail" v-for="item in listAllSolutions" >
                    <div class="img"><img :src= "'/storage/'+item.iconLink"></div>
                    <div class="txt">
                        <h4 class="title-detail"><a :href="'/giai-phap/'+item.slug">@{{item.title}}</a></h4>
                        <p class="txt-detail">@{{item.description}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-event border-line">
            <h2 class="title-box"><a href="/tin-tuc">{{ __('hompage.newEvent') }}</a></h2>

            <div class="col col-l" v-if="index==0" v-for="(item,index) in listArrNews">
                <a class="banner" :href="'/tin-tuc/'+item.slug"><img :src="'./storage/'+item.imageDisplay"></a>
                <h4 class="title"><a :href="'/tin-tuc/'+item.slug">@{{ item.title }}</a></h4>
                <span v-if="index == 0" class="time">(Đăng ngày: @{{ date(item.created_at) }})</span>
                <p class="txt">@{{ item.shortDescription }}</p>
                <p class="btn-view"><a :href="'/tin-tuc/'+item.slug">{{ __('hompage.seenNextTitle') }}</a></p>
            </div>
            <div class="col col-r">
                <div class="col-row" v-if="index > 0" v-for="(item,index) in listArrNews">
                    <a class="banner" :href="'/tin-tuc/'+item.slug"><img :src="'./storage/'+item.imageDisplay"></a>
                    <div class="row-info">
                        <h4 class="title"><a :href="'/tin-tuc/'+item.slug">@{{ item.title }}</a></h4>
                        <p class="txt">@{{ item.shortDescription }}</p>
                        <p class="btn-view"><a :href="'/tin-tuc/'+item.slug">{{ __('hompage.seenNextTitle') }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    new Vue({
    el: '#app',
    data: {
        listArrNews: [],
        listAllServices: [],
        listAllSolutions: [],
        listAllInstruct: []
    },
    mounted() {
    this.listAllServices = {!! $allService !!},
    this.listAllInstruct = {!! $allInstruct !!},
    this.listAllSolutions = {!! $allSolutiion !!},
    this.listArrNews = {!! $allNews !!}
    },
    methods: {
        date(date) {
            return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
        }
    }
});
</script>  
@endsection
