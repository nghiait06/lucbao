<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use DB;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allservices = DB::table('services')->simplePaginate(10);

        return view ('servicePage',compact('allservices'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }

    public function getAll()
    {
       
        $data=  Service::where("isActive","1")
        ->orderBy('priority', 'asc')
        ->get();
        return $data;

    }

    public function getAllHomePage()
    {
       
        $data=  Service::where("isActive","1")
        ->orderBy('isDisplayHOme','desc')
        ->orderBy('priority', 'asc')
        ->get();
        return $data;

    }


    public function GetDetail($urlName)
    {

        $codeInput =$urlName;
        $item = null;
        if($codeInput)
        {
            $item =  Service::where('slug', $codeInput)
               ->first();
           if($item)
            {
                $convert = str_replace( '\\', '/', $item->backGroundImageLink);
                $item->imageDisplay ='storage/'.$convert;
                // get all danh muc
                $allService=  Service::where("isActive","1")
                ->select('slug','title')
               ->orderBy('priority', 'asc')
                ->get();
              
                $data = [
                    'item'  => $item,
                    'allService'   =>$allService
                ];  
           
                return view('serviceDetailPage', ['data' => $data]);
            }
            else 

            {
                return  "not found";
            }
        }
        else 
        {
            return  "not found";
        }

    }
}
