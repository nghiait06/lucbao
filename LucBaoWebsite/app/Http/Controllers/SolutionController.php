<?php

namespace App\Http\Controllers;

use App\Solution;
use Illuminate\Http\Request;
use DB;

class SolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solutions = DB::table('solutions')->simplePaginate(10);

        return view ('solutionPage',compact('solutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Solution  $solution
     * @return \Illuminate\Http\Response
     */
    public function show(Solution $solution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Solution  $solution
     * @return \Illuminate\Http\Response
     */
    public function edit(Solution $solution)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Solution  $solution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solution $solution)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Solution  $solution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solution $solution)
    {
        //
    }

    public function getAll()
    {
       
        $data=  Solution::where("isActive","1")
        ->orderBy('priority', 'asc')
        ->get();
        return $data;

    }

    public function getAllHomePage()
    {
       
        $data=  Solution::where("isActive","1")
        ->orderBy('isDisplayHOme','desc')
        ->orderBy('priority', 'asc')
        ->get();
        return $data;

    }

    // public function GetDetail(Request $request)
    // {
    //     $code =$request->input('code');
    //     $data =   Solution::where('code', $code)
    //     ->first();
    //     return $data;
    // }

    public function GetDetail($urlName)
    {

        $codeInput =$urlName;
       
        $item = null;
        if($codeInput)
        {
            $item =  Solution::where('slug', $codeInput)
               ->first();
              if($item)
            {
                
                // get all danh muc
                $allSolutions=  Solution::where("isActive","1")
                ->select('slug','title')
                ->orderBy('priority', 'asc')
                ->get();
              
                $data = [
                    'item'  => $item,
                    'allSolution'   =>$allSolutions
                ];  
           
                return view('solutionDetailPage', ['data' => $data]);
            }
            else 

            {
                return  "not found";
            }
        }
        else 
        {
            return  "not found";
        }

    }

}
