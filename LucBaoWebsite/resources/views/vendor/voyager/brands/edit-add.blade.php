@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->display_name }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add')])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;
		
		 var allProductCategory =[];
		 
		 var productCategoryLevel1 = [];
		 var productCategoryLevel2 = [];
		 var productCategoryLevel3 = [];
		 var productCategoryLevel4 = [];
		

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
			
			
			
			$.ajax({  
           type: "GET",  
           url: "/api/getAllProductCategory",  
           data: "{}",  
           success: function (data) {  
              
			  allProductCategory = data;
			  data.forEach(function(item, index, array) {
					
					var itemlevel1 = item;
					// itemlevel1.children =  [];
					productCategoryLevel1.push(item);
					
					if( item.children.length >0)
					{
						 item.children.forEach(function(item2, index, array) {
							 
							 productCategoryLevel2.push(item2);
							 if( item2.children.length >0)
							 {
							       item2.children.forEach(function(item3, index, array) {
									
									    item3.parrentkey1 = item.id;
									    productCategoryLevel3.push(item3);
										
									});
							
							 }
								 
							 
						});
					}
					
				});
			 
		
			
			    
			  var txtSelectDropdownlist1 = '<option value="">Select Category1</option>';
			
      
				productCategoryLevel1.forEach(function(category12, index, array) {
					
				txtSelectDropdownlist1+= '<option value='+ '"'+category12.id +'"'+'>'+category12.name+'</option>';
					
				});
				
				var category1 = document.getElementById('category1');
				category1.innerHTML  ="";
				category1.innerHTML  +=txtSelectDropdownlist1; 
				
			
			 var txtSelectDropdownlist2 =  '<option value="">Select Category2</option>';
			  var parrentkey = -1;
			 productCategoryLevel2.forEach(function(category13, index, array) {
				if(parrentkey !=  category13.parrentkey)
				{    
			console.log(parrentkey);
			      if(parrentkey >0)
				  {
					   
					   
					  if(!txtSelectDropdownlist2.endsWith('</optgroup>'))
					  { 
						   txtSelectDropdownlist2 += '</optgroup>';
					  }
				  }
 				  txtSelectDropdownlist2+=' <optgroup data-rel="'+ category13.parrentkey+'">';
				  parrentkey = category13.parrentkey;
				}

				else  
				{
					txtSelectDropdownlist2 +='<option value='+ '"'+category13.id +'"'+'>'+category13.name+'</option>';
				}	
			  });
			   txtSelectDropdownlist2 += '</optgroup>';
			  	var category2 =document.getElementById("category2");
				category2.innerHTML  ="";
				category2.innerHTML  +=txtSelectDropdownlist2; 
				console.log(txtSelectDropdownlist2);
				


			var txtSelectDropdownlist3 =  '<option value="">Select Category3</option>';
			  var parrentkey4 = -1;
			 productCategoryLevel3.forEach(function(category14, index, array) {
				if(parrentkey4 !=  category14.parrentkey1)
				{    

			      if(parrentkey4 >0)
				  {
					   
					   
					  if(!txtSelectDropdownlist3.endsWith('</optgroup>'))
					  { 
						   txtSelectDropdownlist3 += '</optgroup>';
					  }
				  }
 				  txtSelectDropdownlist3+=' <optgroup data-rel="'+ category14.parrentkey1+'">';
				  parrentkey4 = category14.parrentkey1;
				}

				else  
				{
					txtSelectDropdownlist3 +='<option value='+ '"'+category14.id +'"'+'>'+category14.name+'</option>';
				}	
			  });
		
			  	var category3 =document.getElementById("category3");
				category3.innerHTML  ="";
				category3.innerHTML  +=txtSelectDropdownlist3; 
				
				
				
			 triigerEvenetClick();
			 
			 readValue();
				

           }  
       });  
			
			
        });
		
		
	function triigerEvenetClick() {
		
		var $cat = $("#category1"),
        $subcat = $(".subcat");
		
		var $category2 =$("#category2");
		var $category3 =$("#category3");
		
    
    var optgroups = {};
    
    $subcat.each(function(i,v){
    	var $e = $(v);
    	var _id = $e.attr("id");
			optgroups[_id] = {};
			$e.find("optgroup").each(function(){
      	var _r = $(this).data("rel");
        $(this).find("option").addClass("is-dyn");
      	optgroups[_id][_r] = $(this).html();
			});
    });
    $subcat.find("optgroup").remove();
    
    var _lastRel;
    $cat.on("change",function(){
        var _rel = $(this).val();
        if(_lastRel === _rel) return true;
        _lastRel = _rel;
        $subcat.find("option").attr("style","");
        $subcat.val("");
        $subcat.find(".is-dyn").remove();
        if(!_rel) return $subcat.prop("disabled",true);
        $subcat.each(function(){
        	var $el = $(this);
          var _id = $el.attr("id");
          $el.append(optgroups[_id][_rel]);
        });
        $subcat.prop("disabled",false);
		
		$("#valuecode").val(_rel);
    });
	
	    $category2.on("change",function(){
        var _rel = $(this).val();
        if(_rel != "")
		{
		  	$("#valuecode").val(_rel);
			
		}
		else 
		{
			$("#valuecode").val("");
		}
		});
		
		$category3.on("change",function(){
        var _rel = $(this).val();
        if(_rel != "")
		{
		  	$("#valuecode").val(_rel);
			
		}
		else 
		{
			$("#valuecode").val(_rel);
		}
		});
		
	}
	
	function  readValue() 
	{
		var valuemember = $("#valuecode");
		
		var val = valuemember.val();
		if(!val)
		{
			return;
		}
		console.log(val);
		var categorySelect3 = -1;
		var categorySelect2 = -1;
		var categorySelect1 = -1;
	    var productcategoryStanding3 =productCategoryLevel3.filter(item=>item.id ==val)[0];
		    var productcategoryStanding2;
			var productcategoryStanding1;
		
		console.log(productcategoryStanding3);
		if( productcategoryStanding3)
		{
			
			categorySelect2 = productcategoryStanding3.parrentkey;
			categorySelect1 = productcategoryStanding3.parrentkey1;
			categorySelect3=  val;
			
		}
		else 
		{
			
			productcategoryStanding2 =  productCategoryLevel2.filter(item=>item.id ==val)[0];
			
			if(productcategoryStanding2)
			{   

				categorySelect1 = productcategoryStanding2.parrentkey;
				categorySelect3 =-1;
				categorySelect2= val;
			}
			else 
			{
				categorySelect1 = val;
				categorySelect3 = val;
				categorySelect2 = val;
				
			}
			
			
		}
		
		
		
		console.log(categorySelect1);
		console.log(categorySelect2);
		console.log(categorySelect3);
		$('#category1').val(categorySelect1).change();
		
		$('#category2').val(categorySelect2).change();
		
		$('#category3').val(categorySelect3).change();
	}
		
	
 </script>
@stop
