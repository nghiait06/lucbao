@extends('layout.layout')
@section('title',  __('hompage.contactTitle'))
@section('content')

<?php 
$address = Voyager::setting('site.Address', 'default-value'); 
$companyName = Voyager::setting('site.companyName', 'default-value'); 
$email = Voyager::setting('site.Email', '');
$phoneNumber = Voyager::setting('site.PhoneNumber', '');
?>
<div class="content contact">
    <div class="container">
        <div class="box border-line">
            <h4 class="title">{{ __('hompage.contactTitle') }}</h4>
            <div class="col-row">
                <div class="col-l">
                    <div class="form">
                        <input type="text" v-model="fullName" :class="{errBorder: email == '' && validator}" placeholder="{{ __('hompage.fullName') }}">
                        <input type="text" v-model="email" :class="{errBorder: email == '' && validator}" placeholder="{{ __('hompage.EmailTitle') }}">
                        <input type="text" v-model="phone" :class="{errBorder: phone == '' && validator}" placeholder="{{ __('hompage.PhoneNumberTitle') }}">
                        <input type="text" v-model="messFeedBack" :class="{errBorder: messFeedBack =='' && validator}" placeholder="{{ __('hompage.MesaageConatact') }}">
                        <div class="submit">
                            <input type="submit" value="{{ __('hompage.MesaageConatact') }}" @click="submitFeedback"/>
                            <img src="./assets/images/icon_arrow_product.png">
                        </div>
                    </div> 
                </div>
                <div class="col-r">
                    <h4 class="name">{{ __('hompage.CompanyTile') }}</h4>
                    <p>
                        <span> {{ __('hompage.AdrressTitle') }} </span>
                        <span>{{$address}}</span>
                    </p>
                    <p>
                        <span> {{ __('hompage.PhoneNumberTitle') }} </span>
                        <span>{{$phoneNumber}}</span>
                    </p>
                    <p>
                        <span>{{ __('hompage.EmailTitle') }}</span>
                        <span>{{$email}}</span>
                    </p>
                    <p>
                        <span>Website</span>
                        <span>http://lucbao.nttech.vn/</span>
                    </p>
                    <p>
                        <span>Fanpage</span>
                        <span>http://lucbao.nttech.vn/</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
            fullName: '',
            email: '',
            phone: '',
            messFeedBack: '',
            validator : false
        },
        methods: {
            submitFeedback() {
                this.validator = true;
                if(this.fullName == '' || this.email == '' || this.phone == '' || this.messFeedBack == '') {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.options.closeButton = true;
                    toastr.error(
                        'Xin vui lòng nhập đầy đủ thông tin cần thiết', 
                        'Cảnh báo.', 
                        { timeOut: 3000 }
                    )
                } else {
                    this.validator = false;
                    let body = {
                        fullName: this.fullName,
                        email: this.email,
                        phoneNumber: this.phone,
                        message: this.messFeedBack
                    };
                    axios.post('/feeback/createNew', body)
                    .then(response => {
                        console.log(response);
                        toastr.options.positionClass = 'toast-top-center';
                        toastr.options.closeButton = true;
                        toastr.success(
                            'Cảm ơn đã liên lạc với chúng tôi, sẽ liện hệ lại sớm nhất có thể.', 
                            'Gởi thành công.', 
                            { timeOut: 3000 }
                        );
                        this.fullName = '';
                        this.email = '';
                        this.phone = '';
                        this.messFeedBack = '';
                    })
                    .catch(error => {
                        console.log(error);
                    })
                }
            }
        }
    });
  </script>  
@endsection