<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';
    public function parrentkey() {
        return $this->belongsTo(ProductCategory::class);
        }


        public function parent() {
            
            return $this->hasOne(ProductCategory::class, 'id', 'parrentkey');
          }

          public function children() {
            // return $this->hasMany(static::class, 'parrentkey');
            return $this->hasMany(ProductCategory::class, 'parrentkey', 'id');
          }

          public function allProdcut() {
            return $this->hasMany(Product::class, 'productCategoryCode', 'id');
          }
}
