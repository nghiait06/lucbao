<?php $variable = Voyager::setting('site.taxCode', 'default-value'); 
$address = Voyager::setting('site.Address', 'default-value'); 
$companyName = Voyager::setting('site.companyName', 'default-value'); 
$email = Voyager::setting('site.Email', '');
$phoneNumber = Voyager::setting('site.PhoneNumber', '');
?>
<footer id="footer">
        <div class="container">
            <div class="col-footer">
                <h4 class="title">{{ __('hompage.solutionService') }}</h4>
                <span class="txt"><a href="/dich-vu/bao-tri-may-tinh">Bảo trì máy tính</a></span>
                <span class="txt"><a href="/dich-vu/dich-vu-it-tan-noi">Dịch vụ IT tận nơi</a></span>
                <span class="txt"><a href="/giai-phap/giai-phap-ban-le">Giải pháp bán lẻ</a></span>
                <span class="txt"><a href="/giai-phap/quan-tri-doanh-nghiep-erp">Quản trị doanh nghiệp</a></span>
            </div>
            <div class="col-footer">
                <h4 class="title">{{ __('hompage.aboutUsTitle') }}</h4>
                <span class="txt"><a href="/lich-su-phat-trien-cong-ty">Lịch sử phát triển công ty</a></span>
                <span class="txt"><a href="/tam-nhin-va-phat-trien">Tầm nhìn và phát triển</a></span>
                <span class="txt"><a href="/van-hoa-cong-ty">Văn hóa công ty</a></span>
                <span class="txt"><a href="#">Tuyển dụng</a></span>
            </div>
            <div class="col-footer">
                <h4 class="title">{{ __('hompage.CompanyTile') }}</h4>
                <span class="txt">{{ __('hompage.contactTitle') }}</span>
                <span class="txt">{{ __('hompage.AdrressTitle') }} {{$address}}</span>
                <span class="txt">{{ __('hompage.TaxTitle') }}  {{$variable}}</span>
                <span class="txt">{{ __('hompage.PhoneNumberTitle') }}  {{$phoneNumber}}</span>
                <span class="txt">{{ __('hompage.EmailSupportTitle') }}  {{$email}}</span>
            
            </div>
            <div class="col-footer">
                <h4 class="title">{{ __('hompage.directTionPageTitle') }}</h4>
                <span class="txt"><a href="/san-pham">Sản phẩm</a></span>
                <span class="txt"><a href="/dich-vu">Dịch vụ</a></span>
                <span class="txt"><a href="/giai-phap">Giải pháp</a></span>
                 <span class="txt"><a href="/dao-tao">Đào tạo</a></span>
            </div>
        </div>
    </footer>    