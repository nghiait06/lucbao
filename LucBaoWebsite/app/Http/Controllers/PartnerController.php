<?php

namespace App\Http\Controllers;

use App\Partner;
use App\LoyalCustomer;
use Illuminate\Http\Request;
use URL;


class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getAllItem() {
		$allParner = Partner::all ();
                
		foreach ($allParner as $item) {
		 
		$item->urlImage = URL::asset( "/storage"."/".$item->linkImage);
		}

		$allCustomer =LoyalCustomer::where("isActive",1)->get();


		foreach ($allCustomer as $item) {
		 
		$item->urlImage = URL::asset( "/storage"."/".$item->linkImage);
		}

		$data = [
			'dataCustomer'  => $allParner,
			'dataLoyal'   =>$allCustomer
		];  

		return $data;
	}
	
	
	public function getAll() {
		
		 $data = Partner::all ();
		 foreach ($data as $item) {
		 
		$item->urlImage = URL::asset( "/storage"."/".$item->linkImage);
		
		}
		return $data;
	}



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        //
    }
}
