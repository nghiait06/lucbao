<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use App\Product;
use Illuminate\Http\Request;
use DB;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        //
    }
    
     function  addInfomationToChild( $arrayChild, $parrentCode, $parrentName,$path)
    {
         if(empty($arrayChild->children))
            {
                return;
            }
            foreach ($arrayChild->children as $value1){
                $value1->parrentCode = $parrentCode;
                $value1->parrentName = $parrentName;
                if( $path)
                {

                }
                else 
                {
                    $path = $parrentName;
                }
                $value1->path = $path.'/'.$value1->name;
            
               $this->addInfomationToChild($value1,$value1->code,$value1->name,$value1->path);
            } 

    }
    public function getAll(Request $request)
   {
        $array =  ProductCategory::with('children.children.children.children')
        ->whereNull('parrentkey')
        ->where('status','=',1)
        ->get();
        foreach ($array as $value){

            $this->addInfomationToChild($value,$value->code,$value->name,"");
        }
        // $array = $allProductCategory;
        return $array;
    }
    
	public function GetAllCategory(Request $request)
	{
		 $array =  ProductCategory::with('children.children.children.children')
        ->whereNull('parrentkey')
        ->where('status','=',1)
        ->get();
    
        return $array;
		
	}
 

    public function GetAllProductByCategoryCode(Request $request)
    {
        $codeInputId = $request->input('productCategoryId');
        // $allProductByCategoryId =   ProductCategory::with('allProdcut')
        // ->where('id' ,$codeInputId)
        // ->get();
		
		
        $category = ProductCategory::find($codeInputId);
        // $category->setRelation('lessons', $category->lesson()->paginate(10));
		
		
        $allProduct = Product::query()
        ->where('productCategoryCode', $category->id)
        ->paginate(16);

        return $allProduct;

    }

    function  getAllProductcategoryCode( $arrayChild,$allProductCategoryId)
    {
        
          if(empty($arrayChild->children))
            {
                
                return $allProductCategoryId;
            }
            foreach ($arrayChild->children as $value2){
              
                if( $allProductCategoryId)
                {

                }
                else 
                {
                    // $allProductCategoryId =$value2->id;
                }
                $allProductCategoryId= $allProductCategoryId.';'.$value2->id;
            
                 $this->getAllProductcategoryCode($value2,$allProductCategoryId);
            } 

    }
   
    
    public function getAll1(Request $request)
    {
        $codeInputId = $request->input('productCategoryId');
        $array =  ProductCategory::with('children.children.children.children')
        ->where('id',$codeInputId)->get();
        return  $this->getAllProductcategoryCode($array[0],"");
 

    }
    
}
