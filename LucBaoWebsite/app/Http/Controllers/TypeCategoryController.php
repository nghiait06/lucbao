<?php

namespace App\Http\Controllers;

use App\TypeCategory;
use Illuminate\Http\Request;

class TypeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getAllItem(Request $request)
    {
        $type = $request->input('type');
        $data = TypeCategory::where('type', $type)
         ->orderBy('priority', 'asc')
         ->get();
		return $data;
	}

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeCategory  $typeCategory
     * @return \Illuminate\Http\Response
     */
    public function show(TypeCategory $typeCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeCategory  $typeCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeCategory $typeCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeCategory  $typeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeCategory $typeCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeCategory  $typeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeCategory $typeCategory)
    {
        //
    }
}
