<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
			 $table->bigIncrements('id');
			 $table->string('code',20);
			 $table->string('name',250);
			 $table->longText('description',250)->nullable();
			  $table->boolean('isActive')->default(1);
			  $table->string('status',20)->nullable();
			 $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
