
<?php 
$hotline1 = Voyager::setting('site.HotLine1', ''); 
$hotline2 = Voyager::setting('site.HotLine2', ''); 
$SologanVi = Voyager::setting('site.SologanVi', ''); 
$SologanEn= Voyager::setting('site.SologanEn', ''); 

?>
<div class="menu-header">
            <div class="container">
			@if (App::isLocale('vi'))
				<p class="item">{{$SologanVi}}</p>
			@else
					<p class="item">{{$SologanEn}}</p>
			@endif
              
            </div>
        </div>
        <div class="logo-header">
            <div class="container">
                <div class="logo">
                    <a href="/"><img :src="'/assets/images/logo.png'"></a>
                </div>
                    <div class="search">
                    <input type="search" placeholder= "{{ __('hompage.search') }}">
                    <button><img :src="'/assets/images/icon_search.png'"></button>
                </div>
                <div class="hotline">
                    <p class="img"><img :src="'/assets/images/img_phone.png'"></p>
                    <div>
                        <p class="hot-line">
                         
                            <span class="clr">{{$hotline1}}</span>
                            <span class="clr">{{$hotline2}}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

