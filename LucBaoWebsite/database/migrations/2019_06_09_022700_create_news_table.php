<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
			 $table->string('code',20);
			  $table->string('title',250)->nullable();
			  $table->string('shortDescription',500)->nullable();
			   $table->longText('content')->nullable();
			    
				$table->string('typeNew',100)->nullable();
				 $table->integer('priority')->default(1);
			  $table->mediumText('textDiplay',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
