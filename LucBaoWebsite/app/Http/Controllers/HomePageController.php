<?php

namespace App\Http\Controllers;

use App\News;
use App\Instruct;
use App\Service;
use Illuminate\Http\Request;
use App\Solution;
use App\ProductCategory;
use session;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         

			$allService=  Service::where("isActive","1")
            ->orderBy('isDisplayHOme','desc')
            ->orderBy('priority', 'asc')
            ->take(6)
            ->get();
    
            // get all dao tao
            $allInstruct=  Instruct::where("isActive","1")
            ->orderBy('isDisplayHOme','desc')
            ->orderBy('isHot','desc')
            ->orderBy('isNew','desc')
            ->orderBy('created_at', 'desc')
            ->orderBy('priority', 'asc')
            ->take(6)
            ->get();
    
            // get all tin tuc
            $allNews = News::where('status', 1)
            ->orderBy('updated_at', 'desc')
            ->orderBy('isHomePage', 'desc')
            ->orderBy('priority', 'asc')
            ->take(4)
            ->get();  
            //get all giai phap
    
            $allSolutiion=  Solution::where("isActive","1")
            ->orderBy('isDisplayHOme','desc')
            ->orderBy('priority', 'asc')
            ->take(6)
            ->get();
		
		 
			
            $data = [
                'allService'  => $allService,
                'allNews'   =>$allNews,
                'allInstruct'  => $allInstruct,
                'allSolutiion'   =>$allSolutiion,
				
            ];  
			
		
          
            //  $request->session()->put('homepageData', $data);
            return view('homePage', ['data' => $data]);   
    }

    public function StrategyView(Request $request)
    {
        
            return view('infomationCompany.strategy');   
    }

    public function HistoryView(Request $request)
    {
        
            return view('infomationCompany.history');   
    }
    public function CultureView(Request $request)
    {
        
            return view('infomationCompany.culture');   
    }

}
