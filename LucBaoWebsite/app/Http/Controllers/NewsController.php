<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

use DB;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news =  News::paginate(10);

        return view ('newsEventPage',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
    }

    public function GetDetail($urlName)
    {

        $codeInput =$urlName;

    
        if($codeInput)
        {
            $item =  News::where('slug', $codeInput)
               ->first();
              if($item)
            {

                $relationships=  News::where('code','<>',$item->code)
                ->orderBy('priority', 'asc')
                ->take(6)
                ->get();
          
                $data = [
                    'item'  => $item,
                    'relationships' =>$relationships
                
                ];  
           
                return view('newEventDetailPage', ['data' => $data]);
            }
            else 

            {
                return  "not found";
            }
        }
        else 
        {
            return  "not found";
        }

    }

    public function GetHomePage(Request $request)
    {
        $data = News::where('status', 1)
        ->orderBy('updated_at', 'desc')
        ->orderBy('isHomePage', 'desc')
        ->orderBy('priority', 'asc')
        ->take(4);

        return $data->get();
    }

    public function GetAll(Request $request)
    {
        $conditionFilter =$request->input('filterNews');
        $data = News::where('status', 1);
        if($conditionFilter)
        {
            $data = $data->where('typeNew', $conditionFilter);
        };

       $data = $data->orderBy('updated_at', 'desc')
        ->orderBy('isHomePage', 'desc')
        ->orderBy('priority', 'asc')

        ->take(10);

        return $data->get();
    }


}
