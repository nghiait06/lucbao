<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllTypeLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		 Schema::table('brands', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('customer_satisfactions', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('news', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('instructs', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('products', function (Blueprint $table) {
			$table->string('integer')->nullable()->default(1000);
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('partners', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
			$table->boolean('isActive')->default(1);
		});
			 Schema::table('product_categories', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('reasons', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		
		 Schema::table('services', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		 Schema::table('solutions', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
		 Schema::table('types', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
	
		Schema::table('type_categories', function (Blueprint $table) {
			$table->string('lang')->nullable()->default("vi");
		});
			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
