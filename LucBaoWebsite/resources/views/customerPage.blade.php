@extends('layout.layout')
@section('title', __('hompage.CustomerTitle'))
@section('content')
<div class="content customer">
    <div class="container">
        <div class="box">
               <h4 class="title">{{ __('hompage.CustomerTitle') }}</h4>
            <div class="col-row">
                <div class="col col-4" v-for="item in listData.dataCustomer">
                    <img :src="'./storage/'+item.linkImage">
                    <p class="txt" v-html="item.description"></p>
                </div>
            </div>
        </div>
        <div class="box border-line">
            <h4 class="title">{{ __('hompage.loyalTtile') }}</h4>
            <div class="col-row">
                <div class="col col-4" v-for="item in listData.dataLoyal">
                    <img :src="'./storage/'+item.linkImage">
                    <p class="txt" v-html="item.description"></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
            listData: []
        },
        created() {
            axios.get('/parter/getAllCustomer')
                .then(res => {
                    console.log(res.data);
                    this.listData = res.data;
                }).catch(err => {
                    console.log(err)
                });
        },
    });
  </script>  
@endsection