<header id="header">

<nav id="nav">
<div class="container">
<ul class="menu main-menu">
@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }
  

@endphp
	@foreach ($items as $item)
		
		 
		 
        @php

        $originalItem = $item;
        if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
        }
        $isActive = null;
        $styles = null;
        $icon = null;
		
		
		@endphp
		
		@if ($item->id ==29)
			<li class="main sub" id ="productmenu">
          
				 <a href="{{ url($item->link()) }}" target="{{ $item->target }}" style="{{ $styles }}">
				 <span>{{ $item->title }}</span>
				</a> 
               
            </li>
				
			@continue
		@endif

    @if($originalItem->children->isEmpty() )
    <li class="main">
        
        <a href="{{ url($item->link()) }}" target="{{ $item->target }}" style="{{ $styles }}">
          
            <span>{{ $item->title }}</span>
        </a> 
    </li>
    @endif
	
	
    @if(!$originalItem->children->isEmpty())
     <li class="main sub">
   
    <a href="{{ url($item->link()) }}" target="{{ $item->target }}" >
      
        <span>{{ $item->title }}</span>
    </a>

	 @if(!$originalItem->children->isEmpty())
		 <ul class="sub-menu pos">
	 
    @foreach ($originalItem->children as $item1)
    
       <li><a href="{{ url($item1->link()) }}">  <span>{{ $item1->title }}</span></a>
	    <ul class="sub-menu sub">
	   @foreach ($item1->children as $item2)
			 @if($item2->children->isEmpty())
			 
			  <li><a href="{{ url($item2->link()) }}">  <span>{{ $item2->title }}</span></a> 
			  @endif
			  
		       @if(!$item2->children->isEmpty())
				    <li><a href="{{ url($item2->link()) }}">  <span>{{ $item2->title }}</span></a> 
				    <ul class="sub-menu sub">
			        @foreach ($item2->children as $item3)
				  
                         <li><a href="{{ url($item3->link()) }}"><span>{{ $item3->title }}</span></a></li>
                     
				  @endforeach
     				 </ul>
				 </li>
                      
			   @endif
             </li> 
	   @endforeach
		</ul>
	   </li>
	   
    @endforeach
		</ul> 
	 @endif
		 
   
</li>
@endif
  
@endforeach

<li class="main sub flag">
	@if (App::isLocale('en'))
		<a  class="setLang"><img id="setFlag" src="/assets/images/flag_en.png"></a>
	@else
        <a  class="setLang"><img id="setFlag" src="/assets/images/flag_vi.png"></a>
    @endif
	
    <ul class="sub-menu pos">
        <li>
            <a href="/locale/en" onclick="setLanguage('en')"><img class="usa" src="/assets/images/flag_en.png"> English</a>
        </li>
        <li>
            <a href="/locale/vi" onclick="setLanguage('vi')"><img class="vn" src="/assets/images/flag_vi.png"> Tiếng việt</a>
        </li>
    </ul>
</li>
<li class="flag hamburger" onclick="showMenu()">
    <img :src="'/assets/images/icon_hamburger.png'">
</li>
</ul>
    </div>
    </nav>
</header> 