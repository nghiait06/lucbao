<?php

namespace App\Http\Controllers;

use App\FeedBackContact;
use Illuminate\Http\Request;
use Validator;
use Response;
class FeedBackContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
	   $input = $request->all();

        $validatedData = Validator::make($input, [ 
            'fullName' => 'required|max:255',
            'email' => 'required|email',
            'phoneNumber' => 'required',
            'message' => 'required',
        ]);
        if ($validatedData->fails()) { 
            return response()->json(['error'=>$validatedData->errors()], 505);            
        }
        $contactInsert =  new  FeedBackContact();
        $contactInsert->fullName = $input["fullName"];
        $contactInsert->email = $input["email"];
        $contactInsert->phoneNUmber = $input["phoneNumber"];
        $contactInsert->message = $input["message"];
		$contactInsert->isActive = 1;
        $contactInsert->status = "pending";
		$saved =  $contactInsert->save();
		if(!$saved){
       
        }
        else 
        {
        return   Response::json(array('success' => $saved), 200);
        }
	   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeedBackContact  $feedBackContact
     * @return \Illuminate\Http\Response
     */
    public function show(FeedBackContact $feedBackContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeedBackContact  $feedBackContact
     * @return \Illuminate\Http\Response
     */
    public function edit(FeedBackContact $feedBackContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeedBackContact  $feedBackContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeedBackContact $feedBackContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeedBackContact  $feedBackContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeedBackContact $feedBackContact)
    {
        //
    }
}
