<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',250);
            $table->string('code',20);
            $table->decimal('prices', 9, 2)->default('0');
            $table->decimal('pricesSale', 9, 2)->default('0');
            $table->string('imageDisplay',250)->nullable();
            $table->longText('desCription')->nullable();
            $table->longText('longdescription')->nullable();
            $table->longText('techdescription')->nullable();
            
            $table->string('brandCode',20)->nullable();
            $table->string('productCategoryCode',20)->nullable();
            $table->boolean('isSalest')->default('0');
            $table->string('status',20)->nullable();
            $table->boolean('isActive')->default('1');
            $table->longText('imageDetail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
