<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedBackContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_back_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullName',60)->nullable();
			$table->string('email',60)->nullable();
			$table->string('phoneNUmber',60)->nullable();
			$table->longText('message')->nullable();
			$table->boolean('isActive')->default(1);
			$table->string('status',20)->default("Pending");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_back_contacts');
    }
}
