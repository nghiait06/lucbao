@extends('layout.layout')
@section('title',  __('hompage.SolutionShortTitle') )
@section('content')
<div class="content solution">
    <div class="container">
        <div class="box border-line">
            <h4 class="title">{{ __('hompage.SolutionShortTitle') }}</h4>
            <div class="col-box">
            @foreach($solutions as $item)
                   <div class="col-row">
                  
                    <a class="img" href="/giai-phap/{{$item->slug}}"><img src="/storage/{{$item->imageDisplay}}"></a>
                    <h4 class="title-row"><a  href="/giai-phap/{{$item->slug}}">{{ $item->title }}</a></h4>
                    <p class="txt">{{ $item->description }}</p>
                    <a class="view"  href="/giai-phap/{{$item->slug}}">{{ __('hompage.seenNextTitle') }}</a>
                 
                </div>
                @endforeach
            </div>

            {{ $solutions->links() }}
        </div>
    </div>
</div>


@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
         
        },
        created() {
        
        },
    });
  </script>  
@endsection