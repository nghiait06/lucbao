@extends('layout.layout')
@section('title', __('hompage.ServiceShortTitle'))

@section('content')
<div class="content service">
    <div class="container">
        <div class="box border-line">
            <h4 class="title">{{ __('hompage.ServiceShortTitle') }}</h4>
            <div class="col-box" >

            @foreach($allservices as $item)
             @if($loop->index%2 == 0)
                 <div class="col-mg">
                    <div class="col-row">
                        <p class="img"><a href="/dich-vu/{{$item->slug}}"><img src="/storage/{{$item->imageDisplay}}"></a></p>
                    </div>
                    <div class="col-row">
                        <h4 class="title-row"><a href="/dich-vu/{{$item->slug}}">{{ $item->title }}</a></h4>
                        <p class="detail" >
                        {!!$item->description!!}
                          </p>
                        <a class="view" href="'/dich-vu/'+item.slug">{{ __('hompage.seenNextTitle') }}</a>
                    </div>
                </div>
            
            
            @else
                <div class="col-mg">
                   <div class="col-row col-right">
                    <h4 class="title-row"><a href="/dich-vu/{{$item->slug}}">{{ $item->title }}</a></h4>
                        <p class="detail" >
                            {!!$item->description!!}
                        </p>
                        <a class="view" href="'/dich-vu/'+item.slug">{{ __('hompage.seenNextTitle') }}</a>
                    </div>
                    <div class="col-row">
                        <p class="img"><a href="/dich-vu/{{$item->slug}}"><img src="/storage/{{$item->imageDisplay}}"></a></p>
                    </div>
                </div>
            @endif

            @endforeach


         
                
            </div>

            {{ $allservices->links() }}
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
          
        },
        mounted() {
  
 
    },
        methods: {
            setIndex(n) {
                if(n % 2 == 0) {
                    return 'even';
                } else {
                    return 'odd';
                }
            }
        }
    });
  </script>  
@endsection