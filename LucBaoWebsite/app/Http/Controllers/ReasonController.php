<?php

namespace App\Http\Controllers;

use App\Reason;
use App\CustomerSatisfaction;
use Illuminate\Http\Request;
use URL;
class ReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {

       $allReason = Reason::where('isActive', 1)
               ->orderBy('priority', 'asc')
               ->take(3)
               ->get();
         
     foreach ($allReason as $item) {
         
           $item->urlImage = URL::asset( "/storage"."/".$item->imageLink);
      }


      $allCustomerSatisfaction = CustomerSatisfaction::where('isActive', 1)
      ->get();
      foreach ($allCustomerSatisfaction as $item) {
         
        $item->urlImage = URL::asset( "/storage"."/".$item->imageLink);
   }

         $data = [
            'allReason'  => $allReason,
            'allCustomerSatisfaction'   =>$allCustomerSatisfaction
        ];       
       return   $data;      
    }

}
