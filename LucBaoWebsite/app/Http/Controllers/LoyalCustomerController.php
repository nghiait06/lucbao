<?php

namespace App\Http\Controllers;

use App\LoyalCustomer;
use Illuminate\Http\Request;

class LoyalCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoyalCustomer  $loyalCustomer
     * @return \Illuminate\Http\Response
     */
    public function show(LoyalCustomer $loyalCustomer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoyalCustomer  $loyalCustomer
     * @return \Illuminate\Http\Response
     */
    public function edit(LoyalCustomer $loyalCustomer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoyalCustomer  $loyalCustomer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoyalCustomer $loyalCustomer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoyalCustomer  $loyalCustomer
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoyalCustomer $loyalCustomer)
    {
        //
    }
	
	 public function getAllItem() {
		$data = LoyalCustomer::all ();	   
		foreach ($data as $item) {
			$item->urlImage = URL::asset( "/storage"."/".$item->linkImage);
		}
		return $data;
	}
}
