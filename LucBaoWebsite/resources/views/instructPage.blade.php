@extends('layout.layout')
@section('title', 'Đào tạo')
@section('content')
<div class="content service">
    <div class="container">
        <div class="box border-line">
            <h4 class="title">{{ __('hompage.EducationShortTitle') }}</h4>            
            <div class="col-box">
                @foreach($instructs as $item)
                @if($loop->index%2 == 0)
                <div class="col-mg">
                    <div class="col-row">
                        <p class="img"><a href="/dao-tao/{{$item->slug}}"><img src="/storage/{{$item->imageDisplay}}"></a></p>
                    </div>
                    <div class="col-row">
                        <h4 class="title-row"><a href="/dao-tao/{{$item->slug}}">{{ $item->title }}</a></h4>
                        <p class="detail" >
                        {!!$item->description!!}
                          </p>
                        <a class="view" href="'/dao-tao/'+item.slug">{{ __('hompage.seenNextTitle') }}</a>
                    </div>
                </div>

                @else
                <div class="col-mg">
                   <div class="col-row col-right">
                    <h4 class="title-row"><a href="/dao-tao/{{$item->slug}}">{{ $item->title }}</a></h4>
                        <p class="detail" >
                            {!!$item->description!!}
                        </p>
                        <a class="view" href="'/dao-tao/'+item.slug">Xem tiếp</a>
                    </div>
                    <div class="col-row">
                        <p class="img"><a href="/dao-tao/{{$item->slug}}"><img src="/storage/{{$item->imageDisplay}}"></a></p>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            {{ $instructs->links('paginationCus') }}
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
           
        },
        created() {
         
        },
    });
  </script>  
@endsection