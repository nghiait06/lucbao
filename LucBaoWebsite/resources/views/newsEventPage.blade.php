@extends('layout.layout')
@section('title',  __('hompage.newEvent') )
@section('content')
<div class="content news-events">
    <div class="container">
        <div class="box border-line">
            <h4 class="title">{{ __('hompage.newEvent') }}</h4>
            <div class="col-2 col-row">
            @foreach($news as $item)
            <div class="col">   
                <a class="banner" href="/tin-tuc/{{$item->slug}}"><img src="{{URL::asset( '/storage'.'/'.$item->imageDisplay) }}"></a>
                <div class="row-info">
                    <h4 class="title-info"><a href="/tin-tuc/{{$item->slug}}">{{ $item->title }}</a></h4>
                    <span class="time">{{$item->created_at }}</span>
                    <p class="txt">{{ $item->shortDescription }}</p>
                </div>
            </div>
             @endforeach
          
            </div>
          
           
			{{ $news->links('paginationCus') }}			
        
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
            listData: []
        },
        created() {
           
        },
        methods: {
            date(date) {
                return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
            }
        }
    });
  </script>  
@endsection