@extends('layout.layout')

@section('title', __('hompage.ProductShortTitle'))

@php
    $listCategory =$data["allProductCategory"];
	
	$productCategorySelect = $data["productCategorySelect"];

@endphp

@section('content')
<div class="content product">
    <div class="container">
        <div class="slider-header slider-product"></div>
        <div class="box main-product border-line">
            <div class="overlay"></div>
            <span id="iconHambuger" class="icon-ham" @click="showLeftMenu"><img src="./assets/images/icon_hamburger.png"></span>
            <div class="left-menu">
                <ul v-if="nameProduct" class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/san-pham">Sản phẩm</a></li>
                    <li>@{{ nameProduct }}</li>
                </ul>
                <ul v-else class="breadcrumb">
                    <li><a href="/">{{ __('hompage.HomePageTile') }}</a></li>
                    <li>{{ __('hompage.ProductShortTitle') }}</li>
                </ul>
                <div class="left-dropdown">
                    <div class="menu-drop menu-product">
                        <h4 class="title">{{ __('hompage.CategagoryProductTitle') }}</h4>
                        <nav id="top-nav">
                            <ul class="menu">
                                <li v-for="menu in menuDropdown">
                                    <a :class.number="menu.id" @click="findProduct(menu.id,menu.code,menu.name,menu.path,menu.children,false)"> @{{ menu.name }}
                                        <img class="arrow" v-if="menu.children.length > 0" src="{{URL::asset('./assets/images/icon_arrow_product.png' ) }}">
									
                                        <img v-if="menu.icon != null" class="icon" v-bind:src="'/storage/'+menu.icon">
                                        <img v-else class="icon" src= "{{URL::asset('./assets/images/icon_laptop.png' ) }}">
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li v-for="sub in menu.children">
                                            <a :class.number="sub.id" @click="findProduct(sub.id,sub.code,sub.name,sub.path,sub.children,false)">@{{ sub.name  }}
                                                <img class="arrow" v-if="sub.children.length > 0" src="{{URL::asset('./assets/images/icon_arrow_product.png' ) }}">
                                                <img v-if="menu.icon != null" class="icon" :src="'/storage/'+sub.icon">
                                                <img v-else class="icon" src= "{{URL::asset('./assets/images/icon_laptop.png' ) }}">
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li v-for="sub2 in sub.children">
                                                    <a :class.number="sub2.id" @click="findProduct(sub2.id,sub2.code,sub2.name,sub2.path,sub2.children,false)">@{{ sub2.name }}
                                                        <img class="arrow" v-if="sub2.children.length > 0" src="./assets/images/icon_arrow_product.png">
                                                        <img v-if="menu.icon != null" class="icon" :src="'/storage/'+sub2.icon">
                                                        <img v-else class="icon" src= "{{URL::asset('./assets/images/icon_laptop.png' ) }}">
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="right-product">
                <div v-if="isShowList" class="head-list">
                    <span v-if="pathProduct" class="item-list">@{{ pathProduct }}</span>
                    <span v-else>@{{ nameProduct }}</span>
                </div>
                <div v-if="isShowCate" class="head-sub">
                    <span v-for="item in listSubMenu" @click="findProduct(item.id,item.code,item.name,item.path,item.children,false)">
                        <i>
                            <img v-if="item.logo != null" :src="'/storage/'+item.logo">
                            <img v-else src="/assets/images/fap_223B_221_glam.png">
                        </i>
                        <i>@{{ item.name }}</i>
                    </span>
                </div>
                <div class="box-title">
                    <h4 class="title">{{ __('hompage.CategagoryTitle') }} @{{nameProduct}}</h4>
                    <div class="box-sl">
                        <span class="sort-name">{{ __('hompage.SortTitle') }}:</span>
                        <select class="sl-price">
                            <option value="0">{{ __('hompage.ProductPricesAsc') }}</option>
                            <option value="1">{{ __('hompage.ProductPricesDsc') }}</option>
                        </select>
                    </div>
                </div>
                <div class="box-product">
                    <div class="col" v-for='item in listProducts' :key='item.id'>
                         <a :href="'/san-pham/'+item.slug" class="img">
                            <span class="new-item">New</span>
                            <span class="sale" v-if="item.pricesSale >0">Giảm +@{{item.pricesSale*1}}</span>
                            <img :src="'/storage/'+(item.imageDisplay)">
                        </a>
                       <a :href="'/san-pham/'+item.slug" class="title"> @{{item.title}}</a>
                        <p class="price">
                            <span class="price-new" v-if="item.prices - item.pricesSale >0"> 
                                @{{addCommas(item.prices - item.pricesSale)}}
                            </span>
                            <span class="price-new" v-else>Liên hệ</span>
							
                            <span class="price-old" v-if="item.prices - 0 >0"> 
                                @{{addCommas(item.prices - 0)}}
                            </span>
                        </p>
                    </div>
					
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    var allCategories = <?=json_encode($data["allProductCategory"])?>;
    var allBrands = <?=json_encode($data["allBrand"])?>;
    var productCategoryCodeSelect = "";
    var brandCodeSelect ="";
    var productCategorySelect = <?=json_encode($data["productCategorySelect"])?>;

    new Vue({
        el: '#app',
        data: {
            listProducts: [],
            allCategories: allCategories,
            allBrands: allBrands,
            demo: [],
            showBrand: false,
            codeProduct: '',
            nameProduct: '',
            pathProduct: '',
            listSubMenu: [],
            isShowList: false,
            isShowCate: false,
            menuDropdown: []
        },
        created(){
            this.allBrands.forEach((value,key) => {
                this.$set(this.demo,key,{
                    'brandCode': value.code,
                    'brandName': value.name
                });
            });
            this.getAllSpecial();
            this.getalproduct();
            this.getDataLeftMenu();
            this.getAllProductByCategoryCode(productCategorySelect != null ? productCategorySelect[0].id : 0, true);
        },
        methods: {
            showLeftMenu() {
                $(".left-menu").slideToggle(400);
                $(".overlay").toggle(400);
            },
            getAllSpecial(){
                axios.get('api/product/getAllSpecial')
                .then(response => {
                    console.log(response.data);
                    response.data.forEach(el => {
                        $(".slider-product").append(
                            "<div class='slide'>" +
                                "<a href=/san-pham/" + el.slug + ">" +
                                    "<img src=/storage/" + el.imageDisplay + ">" +
                                "</a>" +
                            "</div>"
                        );
                    });
                    $('.slider-product').slick({
                        dots: true,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        adaptiveHeight: true
                    });
                    $('.slider-product').show();
                })
                .catch(error => {
                    console.log(error);
                })
            },
            getalproduct(productCategoryCodeSelect = "", brandCodeSelect = "") {
                axios.get('/product/getall?productCategoryCode='+productCategoryCodeSelect + '&brandCode=' +brandCodeSelect)
                .then(response => {
                    this.listProducts = response.data;

                    if(productCategoryCodeSelect != "") {
                    
                        this.demo = this.removeDuplicates(response.data,"brandCode")
                       
                    }
                    
                })
                .catch(error => {
                    console.log(error);
                })
            },
            getDataLeftMenu() {
                axios.get('/productCategory/getall')
                .then(response => {
                    this.menuDropdown = response.data;
                    var that = this;
                    $.each(response.data, function(key, value) {
                        let id = productCategorySelect != null ? productCategorySelect[0].id : 0
                        if(value.id == id) {
                            that.findProduct(value.id,value.code,value.name,value.path,value.children, true);
                        }
                    });
                })
                .catch(error => {
                    console.log(error);
                })
            },
            removeDuplicates(object,key) {
                if(object.length < 1) {
                    return []
                }
                let x;
                const len=object.length;
                const obj=[];
                for (x=0; x<len; x++) {
                    var isExit = false;
                    for( i =0; i<obj.length; i++)
                    {
                    if( object[x][key] == obj[i][key])
                        isExit =true;
                    }
                    if(!isExit)
                    {
                    obj.push(object[x]);
                    }
                }
                return obj;
            },
            addCommas(nStr) {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + '.' + '$2');
                }
                return x1 + x2;
            },
            filterProduct(code,key) {
                var codeProduct = '';
                this.showBrand = true;
              
                if(key == 'product') {
                    productCategoryCodeSelect = code;
                    brandCodeSelect ="";
                
                // this.getalproduct(productCategoryCodeSelect,brandCodeSelect);
               
                } else {
                    brandCodeSelect = code;
                }
                this.getalproduct(productCategoryCodeSelect,brandCodeSelect);
                console.log(productCategoryCodeSelect);
                console.log(brandCodeSelect);
            },
            findProduct(id,code,name,path,listSub,process) {
                this.isShowList = true;
                this.codeProduct = code;
                this.nameProduct = name;
                this.pathProduct = path;
                this.listSubMenu = listSub;
                if(this.listSubMenu.length > 0) {
                    this.isShowCate = true;
                } else {
                    this.isShowCate = false;
                    if ($(window).width() < 768) {
                        $(".left-menu").hide(400);
                        $(".overlay").hide(400);
                    }
                }
                this.getAllProductByCategoryCode(id, false);
                if(process) {
                    this.openDropdownMenu(productCategorySelect != null ? productCategorySelect[0].id : 0, process);
                }
            },
            getAllProductByCategoryCode(id,process) {
                axios.get('/productCategory/getAllProductByCategoryCode?productCategoryId='+id)
                .then(response => {
                    this.listProducts = response.data.data;
                    this.openDropdownMenu(productCategorySelect != null ? productCategorySelect[0].id : 0, process);
                })
                .catch(error => {
                    console.log(error);
                })
            },
            openDropdownMenu(id,isShow) {
                if(isShow) {
                    var listID = $('#top-nav li > a');
                    for(let i = 0; i < listID.length; i++) {
                        if(id == $(listID[i]).attr('class')) {
                            $(listID[i]).parent().find('ul').first().show(300);
                            $(listID[i]).parents(".dropdown-menu").first().show(300);
                            $(listID[i]).css('background-color','#f1f1f1');
                        }
                    }
                }
            }
        }
    });
</script>  
@endsection
