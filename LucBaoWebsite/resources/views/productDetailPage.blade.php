@php
$item  = $data["item"];
$allProductRelationship = $data["allProductRelationship"];
$allProductNews = $data["allProductNews"];

@endphp
@extends('layout.layout')
@section('title', $item->title)

@section('content')
<div class="content productDetail">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/">Trang chủ</a></li>
            <li><a href="/Product">Sản phẩm</a></li>
            <li>@{{ product_categoriesName }}</li>
        </ul>
        <h4 class="title">@{{ title }}</h4>
        <div class="box-info">
            <div class="box-slider">
   
                <div class="slider-for">
                    <div class="item-for" v-for="link in listImages">
                        <a class="fancybox" :href="'/storage/'+link.url" data-fancybox-group="gallery"><img :src="'/storage/'+link.url"></a>
                    </div>
                </div>
                <div class="slider-nav">
                    <div class="item-nav" v-for="link in listImages">
                        <span class="img"><img :src="'/storage/'+link.url"></span>
                    </div>
                </div>
            </div>
            <div class="box-detail">
                <div v-if="!isContact">
                    <p v-if="isPriceSale" class="price">
                        <span class="new">@{{ price }}</span>
                    </p>
                    <p v-else class="price">
                        <span class="new">@{{ priceSale }}</span>
                        <span v-show="isPriceSale" class="old">@{{ price }}</span>
                    </p>
                    <p class="code">Mã sản phẩm: <span>@{{productCode}}</span></p>
                </div>
                <div v-else>
                    <p class="code">Mã sản phẩm: <span>@{{productCode}}</span></p>
                </div>
                <div class="list-info" v-html="desCription">
				
				</div>
                <div v-if="isContact" class="contact">
                    <span>Liên hệ</span>
                </div>
            </div>
        </div>
        <div class="box-decription border-line">
            <div class="col-box col-intro">
                <div class="tabs">
                    <div class="nav">
                        <button v-for="(tab, index) in tabs" :class="{ active: currentTab === index }" @click="currentTab = index">@{{ tab }}</button>
                    </div>
                    <div class="tab-content">
                        <div v-show="currentTab === 0">
                            <div class="col-content" v-html="longdescription"></div>
                        </div>
                        <div v-show="currentTab === 1">
                            <div class="table" v-html="solutionDescription"></div>
                        </div>
                        <div v-show="currentTab === 2">
							<div class="table" v-html="techdescription"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-box col-params">
                <h4>Sản phẩm mới nhất</h4>
                <div class="box-col">
                    @foreach($allProductNews as $itemnews)
                    <div class="col">
                        <div class="img">
                            <a href="/san-pham/{{$itemnews->slug}}">
                                <span class="new-item">New</span>
                                <img src="/storage/{{$itemnews->imageDisplay}}">
                            </a>
                        </div>
                        <div class="detail">
                            <a href="/san-pham/{{$itemnews->slug}}">{{$itemnews->title}}</a>
                            <p class="price">
                            @if ( $itemnews->prices-$itemnews->pricesSale >0)
                            <span class="price-new"><?php echo number_format($itemnews->prices-$itemnews->pricesSale,0,",",".")?> đ</span>
                             @endif
                              @if ( $itemnews->pricesSale >0)
                                 <span class="price-old"><?php echo number_format($itemnews->prices-$itemnews->pricesSale,0,",",".")?> đ</span>
                               @endif
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="box item-related border-line">
            <h2 class="title-box">Sản phẩm liên quan</h2>
            <div class="box-product slider-product-detail">
   
            @foreach($allProductRelationship as $itemRelation)
              <div class="col">
                    <a href="/product/{{$itemRelation->code}}" class="img">
                        <span class="new-item">New</span>
                        <img src="/storage/{{$itemRelation->imageDisplay}}">
                    </a>
                    <a href="/san-pham/{{$itemRelation->slug}}" class="title">{{$itemRelation->title}}</a>
                    <p class="price">
                        @if ( $itemRelation->prices < 1)
                        <span class="price-new">Liên hệ</span>
                        @endif
                        @if ( $itemRelation->prices-$itemRelation->pricesSale >0)
                        <span class="price-new"><?php echo number_format($itemRelation->prices-$itemRelation->pricesSale,0,",",".")?> đ</span>
                        @endif
                        @if ( $itemRelation->pricesSale >0)
                        <span class="price-old"><?php echo number_format($itemRelation->prices,0,",",".")?> đ</span>
                        @endif
                    </p>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
   
    new Vue({
        el: '#app',
        data: {
            listImages: [],
            title: '',
            name: '',
            price: '',
            priceSale: '',
            isPriceSale: false,
            desCription: '',
            longdescription: '',
            techdescription: '',
			solutionDescription : '',
            productCode: '',
            product_categoriesName: '',
            isContact: false,
            currentTab: 0,
			imageLink: '',
			tabs: ['Thông số kỹ thuật',' Giải pháp',' Tài liệu liên quan'],
        },
        created() {
			var url = window.location.href;
			var param = url.split('/');
			var code = param.pop();
            axios.get('/product/GetDetail?code='+ code)
                .then(res => {
                    this.title = res.data.title;
                    this.price = this.addCommas(res.data.prices * 1);
                    this.priceSale = this.addCommas((res.data.prices * 1) - (res.data.pricesSale * 1));
                    this.isPriceSale = (res.data.pricesSale * 1) == 0 ? true : false;
                    this.isContact = (res.data.prices * 1) == 0 ? true : false;
                    this.desCription = res.data.desCription;
                    this.longdescription = res.data.longdescription;
                    this.techdescription = res.data.techdescription;
                    this.product_categoriesName = res.data.product_categoriesName;
                    this.productCode = res.data.code;
					this.imageLink = res.data.imageDisplay;
					this.solutionDescription = res.data.solutionDescription;
                    var a = JSON.parse(res.data.imageDetail);
                    for(let i = 0; i < a.length; i++) {
                        this.$set(this.listImages,i,{
                            'url': a[i]
                        });
                    }
                    var myVar = setInterval(function() {
                        if($('.slider-for').length > 0 && $('.slider-nav').length > 0) {
                            $('.slider-for').not('.slick-initialized').slick({
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false,
                                fade: true,
                                infinite: false,
                                asNavFor: '.slider-nav',
                                swipe: false
                            });
                            $('.slider-nav').not('.slick-initialized').slick({
                                slidesToShow: 6,
                                slidesToScroll: 1,
                                asNavFor: '.slider-for',
                                dots: false,
                                infinite: false,
                                focusOnSelect: true,
                                responsive: [
                                    {
                                        breakpoint: 1024,
                                        settings: {                                          
                                            slidesToShow: 4
                                        }
                                    },
                                    {
                                        breakpoint: 768,
                                        settings: {                                          
                                            slidesToShow: 4
                                        }
                                    }
                                ]
                            });
                            $('.slider-for').get(0).slick.setPosition();
                            $('.slider-nav').get(0).slick.setPosition();
                            clearInterval(myVar);
                            $(".fancybox").fancybox();
                        }
                    }, 500);
                }).catch(err => {
                    console.log(err)
                });
        },
        methods: {
            addCommas(nStr) {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + '.' + '$2');
                }
                return x1 + x2;
            }
        }
    });
</script>  
@endsection