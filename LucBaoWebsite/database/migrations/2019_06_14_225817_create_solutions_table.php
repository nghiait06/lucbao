+<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solutions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',250)->nullable();
            $table->string('code',20)->nullable();
            $table->longText('description',250)->nullable();
            $table->boolean('isActive')->default(1);
            $table->string('imageDisplay',250)->nullable();
            $table->longText('content')->nullable();
            $table->integer('priority')->default(1);
            $table->boolean('isDisplayHOme')->default(0);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solutions');
    }
}
