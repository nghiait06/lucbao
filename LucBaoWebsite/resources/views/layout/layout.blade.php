<?php 

$facebookSource = Voyager::setting('site.facebookSetting', ''); 

?>
 <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script type="text/javascript" src="{{ URL::asset('/assets/js/library/JQuery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/toastr.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('/assets/js/moment.js') }}"></script>
	<script src="{{ asset('js/app.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('/assets/js/library/slick/slick.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/js/library/slick/slick.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/js/library/slick/slick-theme.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/normalize.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/common.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/shared.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/css/toastr.min.css') }}">
	<script type="text/javascript" src="{{ URL::asset('/assets/js/common.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/moment.js') }}"></script>
    <title>@yield('title') </title>
    <link rel="shortcut icon" type="image/png" href="{{URL::asset('assets/images/logo.ico') }}"/>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/library/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/js/library/fancybox/source/jquery.fancybox.css?v=2.1.5') }}" media="screen">
</head>
<body>
    <div id="app">
   
    <div id="pageTop" class="wrapper">
    <header id="header">
    @component('layout.headerSearch')

    @endcomponent
		{!!menu('menuLucBao' ,'layout.menuLucBao')!!}
    <header>
        
        @yield('content')
        
        <whytous-component title="{{  __('hompage.Whytous') }}" > </whytous-component>
    
        <div class="content">
            <div class="container">
               
               <!-- khach hang  -->
             <parter-component title="{{  __('hompage.customerContact')  }}"> </parter-component>
            </div>
        </div>
        @component('layout.footer')

    @endcomponent
   
     
        
        
    </div>
    </div>

    @yield('js')
    
    <div class="call-phone">
        <div class="phonering-alo-phone phonering-alo-green phonering-alo-show hidden-xs visible-sm visible-md visible-lg" id="phonering-alo-phoneIcon" style="right: -40px; top: 470px; display: block;">
        <div class="phonering-alo-ph-circle"></div>
        <div class="phonering-alo-ph-circle-fill"></div>
        <a href="tel:+84123456789"></a>
        <div class="phonering-alo-ph-img-circle">
            <a href="tel:+84123456789"></a>
            <a href="tel:+84123456789" class="pps-btn-img " title="Liên hệ">
            <img src="./assets/images/v8TniL3.png" alt="Liên hệ" width="50" onmouseover="this.src=&#39;http://assets/images/v8TniL3.png&#39;;" onmouseout="this.src=&#39;http://assets/images/v8TniL3.png&#39;;">
            </a>
        </div>
        </div>
        <div class="phonering-alo-phone phonering-alo-green phonering-alo-show visible-xs hidden-sm hidden-md hidden-lg" id="phonering-alo-phoneIcon" style="right: -40px; top: 70px; display: block;">
        <div class="phonering-alo-ph-circle"></div>
        <div class="phonering-alo-ph-circle-fill"></div>
        <a href="tel:+84123456789"></a>
        <div class="phonering-alo-ph-img-circle">
            <a href="tel:+84123456789"></a>
            <a href="tel:+84123456789" class="pps-btn-img " title="Liên hệ">
            <img src="./assets/images/v8TniL3.png" alt="Liên hệ" width="50" onmouseover="this.src=&#39;http://assets/images/v8TniL3.png&#39;;" onmouseout="this.src=&#39;http://assets/images/v8TniL3.png&#39;;">
            </a>
        </div>
        </div>
    </div>
</body>

{!! $facebookSource !!}
</html>

<script type="text/javascript">
    $(document).ready(function() {
        // $(".main-menu > li").hover(function() {
        //     $(".main-menu li a").removeClass("red");
        //     $(".main-menu li .sub-menu").stop().slideUp(400).height('');
        //     $(this).find('a').first().addClass("red");
        //     $(this).find(".sub-menu").stop().slideDown(400).height('');
        // }, function() {
        //     $(this).find('a').first().removeClass("red");
        //     $(this).find(".sub-menu").stop().slideUp(400).height('');
        // });

        $(document).on("click", "#top-nav li > a", function() {
            $(this).parent().find('ul').first().toggle(300);
            $(this).parent().siblings().find('ul').hide(200);
            $('#top-nav li > a').css('background-color','#fff');
            $(this).css('background-color','#f1f1f1');
        })
		
		var lang = <?=json_encode(app()->getLocale())?>;
		
		$.ajax({  
		
           type: "GET",  
		   
           url: "/api/getAllProductCategory",  
		   
           data: "{}",  
		   
           success: function (data) {   
				var txtmenu = '<ul class="sub-menu pos">';
				data.forEach(function(item, index, array) {
				txtmenu+= "<li>";
				if( lang == "vi")
				{
							txtmenu+= '<a href= "' + '/' +'danh-muc' +'/' +item.slug +'"><span>' +item.name + '</span></a>';
						
				}
				else 
				{
						txtmenu+= '<a href= "' + '/' +'danh-muc' +'/' +item.slug +'"><span>' +item.name + '</span></a>';
				}
				
				 if( item.children.length >0)
				 {
					txtmenu +=  ' <ul class="sub-menu sub">';
					item.children.forEach(function(item2, index2, array2) {
						
						txtmenu+= "<li>";
						txtmenu+= '<a href= "' + '/' +'danh-muc' +'/' +item2.slug +'"><span>' +item2.name + '</span></a>';
						
						 txtmenu +=  ' <ul class="sub-menu sub">';
						if( item2.children.length >0)
						  {
							
							item2.children.forEach(function(item3, index3, array3) {
							txtmenu+= "<li>";
							txtmenu+= '<a href= "' + '/' +'danh-muc' +'/' +item3.slug +'"><span>' +item3.name + '</span></a>';
							txtmenu+= "</li>";
							});
							 
						  }
						   txtmenu+= '</ul>';
					txtmenu+= "</li>";
					});
					 	
				   txtmenu += '</ul>';
				 }
				txtmenu+= "</li>";
		
				});
				txtmenu+= "</ul>";
              var menuproduct  = document.getElementById("productmenu");
			  menuproduct.innerHTML+=txtmenu;
			
		   }  
		   
		   });
    });

    function showMenu() {
        $(".menu > li:not(:first-child,.flag)").slideToggle();        // Function returns the product of a and b
    }

    function setLanguage(val) {
        $('.setLang #setFlag').attr('src', '/assets/images/flag_'+val+'.png');
    }
	
	
</script>