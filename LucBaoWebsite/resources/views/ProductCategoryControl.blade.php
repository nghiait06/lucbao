<select name="category1" id="category1" class ="form-control select2" >

</select>
<label class="control-label" >Chọn danh mục con sản phẩm cấp 2</label>
<select disabled="disabled" class="subcat form-control select2" id="category2" name="category2">

</select>

<label class="control-label" >Chọn danh mục con sản phẩm cấp 3</label>

<select disabled="disabled" class="subcat form-control select2" id="category3" name="category3">

</select>

<input id ="valuecode"  type="hidden"
@if($row->required == 1) required @endif class="form-control" name="{{ $row->field }}"
placeholder="{{ old($row->field, $options->placeholder ?? $row->display_name) }}"
{!! isBreadSlugAutoGenerator($options) !!}
value="{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}">
