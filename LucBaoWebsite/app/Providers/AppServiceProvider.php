<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Service;
use TCG\Voyager\Facades\Voyager;
use App\FormFields\ProductCategoryControlFormField;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
		Voyager::addAction(\App\Actions\MyAction::class);
		
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       Voyager::addFormField(ProductCategoryControlFormField::class);
    }
}
