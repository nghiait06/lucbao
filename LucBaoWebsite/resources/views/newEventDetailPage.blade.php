@extends('layout.layout')
@php
$item  = $data["item"];
$relationships = $data["relationships"]

@endphp
@section('title', $item->title)
@section('content')
<div class="content new-event-detail">
    <div class="container">
        <div class="box border-line">
            <h4 class="title">Chi tiết tin tức - sự kiện</h4>
            <div class="box-detail">
                <h4 class="title-detail">{{ $item->title }}</h4>
                <p class="date-time">
                    <!-- <span class="author">Đức Hải</span> -->
                    <span class="time">Đăng lúc {{ $item->created_at  }}</span>
                </p>
                <div class="decription">
                    <!-- Nơi bind code decription từ server -->
                    <p>{!!$item->content!!}</p>
                </div>
            </div>
        </div>
        <div class="box item-related border-line">
            <h2 class="title-box">Tin tức liên quan</h2>
            <div class="col-2 col-row" >
                @foreach($relationships as $item1)
                <div class="col">
                    <a class="banner" href="/tin-tuc/{{$item1->slug}}"><img src="{{URL::asset( '/storage'.'/'.$item1->imageDisplay) }}" alt=""></a>
                    <div class="row-info">
                        <h4 class="title-info"><a href="/tin-tuc/{{$item1->slug}}">{{ $item1->title }}</a></h4>
                        <span class="txt">{{$item1->shortDescription}}</span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
       new Vue({
        el: '#app',
        data: {
            title: '',
            time: '',
            content: ''
        },
        created() {
            var urlParams = new URLSearchParams(window.location.search);
            axios.get('/news/GetDetail?code='+urlParams.get('detail_code'))
                .then(res => {
                    if(res.data) {
                        this.title = res.data.title;
                        this.time = res.data.created_at;
                        this.content = res.data.content;
                    }
                }).catch(err => {
                    console.log(err)
                });
        },
        methods: {
            date(date) {
                return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
            }
        }
    });
  </script>  
@endsection