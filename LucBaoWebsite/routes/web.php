<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomePageController@index');

// Route::get('/Product', function () {
//     return view('ProductPage');
// });

Route::get('/product-detail', function () {
    return view('productDetailPage');
});
Route::get('/tam-nhin-va-phat-trien','HomePageController@StrategyView');
Route::get('/lich-su-phat-trien-cong-ty','HomePageController@HistoryView');
Route::get('/van-hoa-cong-ty','HomePageController@CultureView');

Route::get('/new-event-detail', function () {
    return view('newEventDetailPage');
});






Route::get('/solution-detail', function () {
    return view('solutionDetailPage');
});

Route::get('/customer', function () {
    return view('customerPage');
});

Route::get('/lien-he', function () {
    return view('contact');
});

Route::get('/test', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get ( '/parter/getall', 'PartnerController@getAll' );
Route::get ( '/parter/getAllCustomer', 'PartnerController@getAllItem' );
Route::get ( '/loyalCustomer/getall', 'LoyalCustomerController@getAllItem' );

Route::get ( '/TypeCategory/getall', 'TypeCategoryController@getAllItem' );


Route::get ( '/news/getall', 'NewsController@GetAll' );
Route::get ( '/news/GetHomePage', 'NewsController@GetHomePage' );
Route::get ( '/tin-tuc/{urlName}', 'NewsController@GetDetail' );
Route::get('/tin-tuc', 'NewsController@index'  );
//product
Route::get ( '/product/getall', 'ProductController@GetAll' );

Route::get ( '/san-pham', 'ProductController@index' );
Route::get ( '/san-pham/{urlName}', 'ProductController@GetDetail1' );
Route::get ( '/danh-muc/{urlName}', 'ProductController@GetDetail' );
Route::get('/product/GetDetail','ProductController@GetDetailProduct');



//solution
Route::get('giai-phap/{urlName}', 'SolutionController@GetDetail');
Route::get ( 'api/solution/getAllHomepage', 'SolutionController@getAllHomePage' );
Route::get ( 'api/solution/getDetail', 'SolutionController@GetDetail' );
Route::get ( 'api/solution/getAll', 'SolutionController@getAll' );
Route::get('/giai-phap', 'SolutionController@index' );

//service

Route::get ( 'api/service/getAllHomepage', 'ServiceController@getAllHomePage' );

 Route::get('dich-vu/{urlName}', 'ServiceController@GetDetail');
Route::get ( 'api/service/getAll', 'ServiceController@getAll' );

Route::get('/dich-vu', 'ServiceController@index');


// Instruct

Route::get ( 'api/Instruct/getAllHomepage', 'InstructController@getAllHomePage' );
Route::get ( 'api/Instruct/getDetail', 'InstructController@GetDetail' );
Route::get ( 'api/Instruct/getAll', 'InstructController@getAll' );
Route::get('dao-tao/{urlName}', 'InstructController@GetDetail');
Route::get('/dao-tao', 'InstructController@index');
// getAllCustomerSatisfaction

Route::get ( '/getAllCustomerSatisfaction', 'ReasonController@getAll' );



Route::get ( '/productCategory/getall', 'ProductCategoryController@getAll' );

Route::get ( '/productCategory/getAllProductByCategoryCode', 'ProductCategoryController@GetAllProductByCategoryCode' );
Route::get ( '/productCategory/getAll1', 'ProductCategoryController@getAll1' );
Route::get ( 'api/product/getAllSpecial', 'ProductController@getAllSpecial' );
Route::get('/resetStorageLink', function () {
    Artisan::call('storage:link');

});


Route::get('/testmenu', function () {
    return view('my_menu');
});

Route::get('/product/clone/{id}', 'VoyagerCustomController@CloneProduct')->name('cloneProduct');


Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});


Route::post('/saveBrand', 'TestController@create');

Route::post('/feeback/createNew', 'FeedBackContactController@store');
Route::get('api/getAllProductCategory', 'ProductCategoryController@GetAllCategory');
