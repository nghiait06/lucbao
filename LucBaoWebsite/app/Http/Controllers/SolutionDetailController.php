<?php

namespace App\Http\Controllers;

use App\SolutionDetail;
use Illuminate\Http\Request;

class SolutionDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SolutionDetail  $solutionDetail
     * @return \Illuminate\Http\Response
     */
    public function show(SolutionDetail $solutionDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SolutionDetail  $solutionDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(SolutionDetail $solutionDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SolutionDetail  $solutionDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SolutionDetail $solutionDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SolutionDetail  $solutionDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(SolutionDetail $solutionDetail)
    {
        //
    }
}
