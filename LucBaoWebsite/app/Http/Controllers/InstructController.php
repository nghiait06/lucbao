<?php

namespace App\Http\Controllers;

use App\Instruct;
use Illuminate\Http\Request;
use DB;

class InstructController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructs = DB::table('instructs')->paginate(10);
  
        return view ('instructPage',compact('instructs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instruct  $instruct
     * @return \Illuminate\Http\Response
     */
    public function show(Instruct $instruct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *  
     * @param  \App\Instruct  $instruct
     * @return \Illuminate\Http\Response
     */
    public function edit(Instruct $instruct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instruct  $instruct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instruct $instruct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instruct  $instruct
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instruct $instruct)
    {
        //
    }

    public function getAll()
    {
       
        $data=  Instruct::where("isActive","1")
        ->orderBy('priority', 'asc')
        ->get();
        return $data;

    }

    public function getAllHomePage()
    {
       
        $data=  Instruct::where("isActive","1")
        ->orderBy('isDisplayHOme','desc')
        ->orderBy('isHot','desc')
        ->orderBy('isNew','desc')
        ->orderBy('created_at', 'desc')
        ->orderBy('priority', 'asc')
        ->get();
        return $data;

    }



    public function GetDetail($urlName)
    {

        $codeInput =$urlName;
       
        $item = null;
        if($codeInput)
        {
            $item =  Instruct::where('slug', $codeInput)
               ->first();
              if($item)
            {
          
                $data = [
                    'item'  => $item,
                
                ];  
           
                return view('instructDetailPage', ['data' => $data]);
            }
            else 

            {
                return  "not found";
            }
        }
        else 
        {
            return  "not found";
        }

    }
}
